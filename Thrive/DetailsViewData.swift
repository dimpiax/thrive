//
//  DetailsViewData.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation

struct DetailsViewData {
    let authorName: String
    let views: Int
}