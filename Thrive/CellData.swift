//
//  CellData.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import FlexibleTableViewController

struct CellData: CellDataProtocol {
    var title: String {
        return authorName
    }
    
    var category: String? {
        return nil
    }
    
    var requestConnector: RequestConnector?
    
    let authorName: String
    let tags: String
    let imagePreviewUrl: String
    let imageUrl: String
    let views: Int
    
    init(requestConnector: RequestConnector?, authorName: String, tags: String, imagePreviewUrl: String, imageUrl: String, views: Int) {
        self.requestConnector = requestConnector
        self.authorName = authorName
        self.tags = tags
        self.imagePreviewUrl = imagePreviewUrl
        self.imageUrl = imageUrl
        self.views = views
    }
    
    func loadImage(string: String, completion: Result<UIImage, NSError> -> Void) {
        requestConnector?.loadImage(string, completion: completion)
    }
}