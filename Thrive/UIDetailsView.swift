//
//  UIDetailsView.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

class UIDetailsView: UIView {
    private let _authorName: UILabel
    private let _viewsField: UILabel
    
    override init(frame: CGRect) {
        self._authorName = UILabel()
        self._viewsField = UILabel()
        
        super.init(frame: CGRect.zero)
        
        backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        addSubview(_authorName)
        addSubview(_viewsField)
        
        _authorName.textColor = .whiteColor()
        _viewsField.textColor = .whiteColor()

        _viewsField.textAlignment = .Right
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        _authorName.frame.origin.x = 15
        _authorName.frame.size = bounds.size
        
        _viewsField.frame.size = bounds.size
        _viewsField.frame.size.width -= 15
    }
    
    func setData(value: DetailsViewData) {
        _authorName.text = "author: \(value.authorName)"
        _viewsField.text = "views: \(value.views)"
    }
}