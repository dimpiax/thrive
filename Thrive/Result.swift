//
//  Result.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

enum Result<T, E: NSError> {
    case Success(T)
    case Failure(E)
    
    init(value: T?, errorCallback: () -> E) {
        self = value.map { .Success($0) } ?? .failure(errorCallback())
    }
    
    init(value: T) {
        self = .Success(value)
    }
    
    init(value: E) {
        self = .Failure(value)
    }
    
    static func success(value: T) -> Result {
        return Result(value: value)
    }
    
    static func failure(value: E) -> Result {
        return Result(value: value)
    }
    
    var value: T? {
        switch self {
            case let .Success(value): return value
            default: return nil
        }
    }
    
    var error: E? {
        switch self {
            case let .Failure(value): return value
            default: return nil
        }
    }
}