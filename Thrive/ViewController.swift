//
//  ViewController.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {
    var model: MainModel!
    
    private var _navVC: UINavigationController!
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.setupListModel("Red roses", query: "red roses")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let listVC = ListViewController()
        listVC.model = model.listModel
        
        _navVC = UINavigationController(rootViewController: listVC)
        addChildViewController(_navVC)
        _navVC.didMoveToParentViewController(self)
        view.addSubview(_navVC.view)
        
        let alertVC = UIAlertController(title: "Thrive!", message: "Welcome to the Thrive app, where you can find pretty flowers to get inspiration of growing flowers in your garden", preferredStyle: .Alert)
        alertVC.addAction(UIAlertAction(title: "Got it!", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
}