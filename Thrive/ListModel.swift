//
//  ListModel.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation

class ListModel {
    var title: String = ""
    var query: String = ""
    
    var requestConnector: RequestConnector?
    
    func requestListData(completion: Result<[CellData], NSError> -> Void) {
        requestConnector?.requestListData(query, completion: completion)
    }
}