//
//  ListViewController.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit
import FlexibleTableViewController

class ListViewController: UIViewController {
    var model: ListModel?
    
    private var _tableViewController: FlexibleTableViewController<CellData, OrderedListGenerator<CellData>>!
    
    override func loadView() {
        super.loadView()
        
        self._tableViewController = FlexibleTableViewController<CellData, OrderedListGenerator<CellData>>(style: .Plain, configuration: TableConfiguation())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let model = model else { return }
        
        // setup title
        title = model.title
        
        // setup list view
        _tableViewController.registerCell(ListTableViewCell.self, reuseIdentifier: ListTableViewCell.reuseIdentifier)
        _tableViewController.requestCellIdentifier = { value in
            return ListTableViewCell.reuseIdentifier
        }
        
        _tableViewController.configureCell = { (cell: UITableViewCell, data: CellData?) in
            guard let data = data else { return false }
            
            if let listCell = cell as? ListTableViewCell {
                listCell.acceptData(data)
            }
            else {
                cell.textLabel?.text = data.title
            }
            
            return true
        }
        
        _tableViewController.cellDidSelect = {[unowned self] value in
            let itemData = self._tableViewController.getItemData(value)
            
            let vc = ImageViewController()
            vc.model = itemData
            self.navigationController?.pushViewController(vc, animated: true)
                        
            return true
        }
        
        addChildViewController(_tableViewController)
        _tableViewController.didMoveToParentViewController(self)
        view.addSubview(_tableViewController.view)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        _tableViewController.view.frame = view.bounds
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let model = model else { return }
        
        model.requestListData {[weak self] result in
            guard let s = self else { return }
            
            if let error = result.error {
                print("Error load: \(error)")
            }
            else {
                s._tableViewController.setData(s.mouldData(result.value!))
            }
        }
    }
    
    private func mouldData(value: [CellData]) -> TableData<CellData, OrderedListGenerator<CellData>> {
        var data = TableData<CellData, OrderedListGenerator<CellData>>(generator: OrderedListGenerator(titlesOrder: { $0.sort(>) }))
        value.forEach { data.addItem($0) }
        data.generate()
        
        return data
    }
}