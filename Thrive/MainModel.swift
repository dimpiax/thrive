//
//  MainModel.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation

class MainModel {
    private var _listModel: ListModel?
    lazy var listModel: ListModel = {
        var model = ListModel()
        model.requestConnector = ServerRequestModel()
        
        return model
    }()
    
    func setupListModel(title: String, query: String) {
        listModel.title = title
        listModel.query = query
    }
}