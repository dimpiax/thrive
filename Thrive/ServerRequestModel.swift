//
//  ServerRequestModel.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

class ServerRequestModel: RequestConnector {
    let host = "https://pixabay.com/api/"
    let apiKey = "2549801-750a2ce11a1b8d8b2dcbf9b28"

    func requestListData(query: String, completion: Result<[CellData], NSError> -> Void) {
        // TODO: refactor under util, like url variables approach
        guard let q = query.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet()) else {
            completion(Result(value: NSError(domain: "Invalid query in requestListData on \(#file)", code: 0, userInfo: nil)))
            return
        }
        
        guard let url = NSURL(string: "\(host)?key=\(apiKey)&q=\(q)&image_type=photo") else {
            completion(Result(value: NSError(domain: "Invalid url in requestListData on \(#file)", code: 0, userInfo: nil)))
            return
        }
        
        let urlSession = NSURLSession.sharedSession()
        let task = urlSession.dataTaskWithURL(url) { (data, urlResponse, error) in
            var err: NSError?
            
            if let _ = error {
                err = error
            }
            else if let response = urlResponse as? NSHTTPURLResponse {
                switch StatusCode(rawValue: response.statusCode) {
                    case .Some(.Success):
                        var parseError: NSError?
                        var json: AnyObject?
                        do {
                            json = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                        } catch let error as NSError {
                            parseError = error
                        } catch {
                            parseError = NSError(domain: "", code: 0, userInfo: nil)
                        }
                        
                        if let jsonData = json as? [String: AnyObject] {
                            guard let hits = jsonData["hits"] as? [AnyObject] else {
                                NSOperationQueue.mainQueue().addOperationWithBlock {
                                    completion(Result(value: NSError(domain: "No 'hits' data in response body on \(#file)", code: 0, userInfo: nil)))
                                }
                                return
                            }

                            let arr = hits.map { value in
                                return CellData(requestConnector: self, authorName: value["user"] as? String ?? "", tags: value["tags"] as? String ?? "", imagePreviewUrl: value["previewURL"] as? String ?? "", imageUrl: value["webformatURL"] as? String ?? "", views: value["views"] as? Int ?? 0)
                            }
                            
                            NSOperationQueue.mainQueue().addOperationWithBlock {
                                completion(Result(value: arr))
                            }
                        }
                        else if let error = parseError { err = error }
                        else { err = NSError(domain: "JSON is invalid", code: 0, userInfo: nil) }
                        
                    default:
                        err = NSError(domain: "Response status is invalid on \(#file). Status: \(response.statusCode)", code: response.statusCode, userInfo: nil)
                    }
            }
            
            if let error = err {
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(Result(value: error))
                }
            }
        }
        task.resume()
    }
    
    func loadImage(string: String, completion: Result<UIImage, NSError> -> Void) {
        guard let url = NSURL(string: string) else {
            completion(Result(value: NSError(domain: "Invalid url in loadImage on \(#file)", code: 0, userInfo: nil)))
            return
        }
        
        let urlSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        let task = urlSession.dataTaskWithURL(url) { (data, urlResponse, error) in
            if let data = data, image = UIImage(data: data) {
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(Result(value: image))
                }
            }
            else {
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    completion(Result(value: NSError(domain: "Didn't recognize image data on \(#file)", code: 0, userInfo: nil)))
                }
            }
        }
        task.resume()
    }
}

enum StatusCode: Int {
    case Success = 200, Invalid = 400, Forbidden = 401
}