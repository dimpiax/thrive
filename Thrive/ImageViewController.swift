//
//  ImageViewController.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

class ImageViewController: UIViewController {
    var model: CellData?
    
    private var _indicatorView: UIActivityIndicatorView!
    private var _imageView: UIImageView!
    private var _detailsView: UIDetailsView!
    
    override func loadView() {
        super.loadView()
        
        self._indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .White)
        self._imageView = UIImageView()
        self._detailsView = UIDetailsView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        view.addSubview(_indicatorView)
        view.addSubview(_imageView)
        view.addSubview(_detailsView)
        
        _indicatorView.color = UIColor.grayColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let model = model else { return }
        
        title = model.tags
        
        _imageView.clipsToBounds = true
        _imageView.contentMode = .ScaleAspectFill
        
        _indicatorView.startAnimating()
        model.loadImage(model.imageUrl) {[weak self] result in
            self?._indicatorView.stopAnimating()
            
            if let image = result.value {
                self?._imageView.image = image
                self?.view.setNeedsLayout()
            }
        }
        
        _detailsView.setData(DetailsViewData(authorName: model.authorName, views: model.views))
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        _imageView.frame = view.bounds
        _detailsView.frame = view.bounds
        _detailsView.frame.size.height = 60
        _detailsView.frame.origin.y = view.bounds.height-60
        
        _indicatorView.center = view.center
    }
    
    deinit {
        model = nil
        
        _indicatorView = nil
        _indicatorView = nil
        _detailsView = nil
    }
}

