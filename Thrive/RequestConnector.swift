//
//  RequestConnector.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

protocol RequestConnector {
    func requestListData(query: String, completion: Result<[CellData], NSError> -> Void)
    func loadImage(string: String, completion: Result<UIImage, NSError> -> Void)
}