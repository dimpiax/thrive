//
//  ListTableViewCell.swift
//  Thrive
//
//  Created by Pilipenko Dima on 5/11/16.
//  Copyright © 2016 dimpiax. All rights reserved.
//

import Foundation
import UIKit

class ListTableViewCell: UITableViewCell {
    class var reuseIdentifier: String {
        return String(self)
    }
    
    let imageWidth: CGFloat = 60
    let textImageOffset: CGFloat = 10
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
        accessoryType = .DisclosureIndicator
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func acceptData(value: CellData) {
        textLabel?.text = "Posted by: \(value.title)"
        detailTextLabel?.text = value.tags
        
        if let imageView = imageView {
            imageView.clipsToBounds = true
            imageView.contentMode = .ScaleAspectFill
            
            value.loadImage(value.imagePreviewUrl) {[weak self] result in
                if let image = result.value {
                    imageView.image = image
                    self?.setNeedsLayout()
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView?.frame.size = CGSize(width: imageWidth, height: bounds.height)
        
        guard let imageView = imageView else { return }
        
        textLabel?.frame.origin.x = imageView.frame.maxX+textImageOffset
        detailTextLabel?.frame.origin.x = imageView.frame.maxX+textImageOffset+1
    }
}
